const express = require('express');
const mongoose = require('mongoose');
const http = require('http');
const app = express();
const router = express.Router();
const routes = require('./routing');
const port = 3000;
const mongoUrl = 'mongodb://localhost:27017/test';

routes(router);

mongoose
    .connect(mongoUrl, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true
    }, (err, db) => {
        if (err) {
            console.error('### DATABASE:\t Unable to connect to server... \n### Error:', err.message);
        } else {
            console.log('### DATABASE:\t Connection established to database at url: ', mongoUrl);
        }
    });

app.use(express.static(__dirname));
app.use('/api', router);
http.createServer(app).listen(port);
console.log('### SERVER:\t\t Running on port ' + port);
