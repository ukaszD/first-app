let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let userSchema = new Schema({
    name: String,
    age: Number
});

userSchema.pre('save', function(next) {
    let error = null;
    //this.name ? error = null : error = new Error("Name missing");
    next(error);
});

let User = mongoose.model('User', userSchema);

module.exports = User;
