let User = require('./models/user.schema');
const TEST_ENDPOINT = 'test';

module.exports = function (router) {
    router.get(`/${TEST_ENDPOINT}`, (req, res) => {
        let newUser = new User();
        newUser.name = "Tomek";
        newUser.age = 34;

        // newUser.save(function (error, result) {
        //     if (error) {
        //         res.status(400).json(error);
        //     } else {
        //
        //         User.find({}).exec(function (error, result) {
        //             if (error) {
        //                 res.status(400).json(error);
        //             } else {
        //                 res.status(200).json(result);
        //             }
        //         });
        //     }
        // });

        User.find({}).exec(function (error, result) {
            if (error) {
                res.status(400).json(error);
            } else {
                res.status(200).json(result);
            }
        });

        // User.find({}).exec(function (error, result) {
        //         if (error) {
        //             res.status(200).json(result);
        //         }
        //         else {
        //             res.status(400).json(error);
        //         }
        //     }
        // );
        // res.status(400).json(returnedError);
    });
};
